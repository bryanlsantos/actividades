// JSON BASE A MOSTRAR EN FORMULARIO
// var baseJSON = {
//     "precio": 0.0,
//     "unidades": 1,
//     "modelo": "XX-000",
//     "marca": "NA",
//     "detalles": "NA",
//     "imagen": "img/default.png"
//   };
var permitir1 = true;
function verifNombre(nombre) {
    if(nombre.value=='')
    {
        let alerta = '<p style="color:orange">El nombre no puede estar vacío<p>';
        $('#aviso-name').html(alerta);
        $('aviso-name').show();
        permitir1 = false;
    }
    else{
        $('aviso-name').hide();
        if(nombre.value != "")
        {
            let alerta = '<p style="color:black">El nombre es permitido<p>';
            $('#aviso-name').html(alerta);
            $('aviso-name').show();
            permitir1 = true;
        }
        else{
            if(nombre.value.length > 100)
            {
                let alerta = '<p style="color:orange">El nombre es muy largo (100 caracteres maximo)<p>';
                $('#aviso-name').html(alerta);
                $('aviso-name').show();
                permitir1 = false;
            }
            else{
                $('aviso-name').hide();
                //true?
            }
        }
    }   
    
}
var permitir12 = true;
function verifNombre2(nombre) 
{
    if($('#name').val()) {
        let search = $('#name').val();
        $.ajax({
            url: './backend/product-search2.php?search='+$('#name').val(),
            data: {search},
            type: 'GET',
            success: function (response) {
                if(!response.error) {
                    // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                    const productos = JSON.parse(response);
                    
                    // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                    if(Object.keys(productos).length > 0) {
                        let alerta = '<p style="color:orange">El nombre ya existe, no puedes ocuparlo!!!<p>';
                        $('#aviso-name').html(alerta);
                        $('aviso-name').show();
                        permitir12 = false;
                    }
                    else
                    {
                        permitir12 = true;
                    }
                }
            }
        })
    }
}

var permitir2 = true;
function verifPrecio(precio) {
    if(precio.value=='')
    {
        let alerta = '<p style="color:orange">El precio no puede estar vacio<p>';
        $('#aviso-precio').html(alerta);
        $('aviso-precio').show();
        permitir2 = false;
    }
    else{
        if(precio.value != "" && precio.value > 99.99)
        {
            let alerta = '<p style="color:black">El precio es valido<p>';
            $('#aviso-precio').html(alerta);
            $('aviso-precio').show();
            permitir2 = true;
        }
        else{
            $('aviso-precio').hide();
        }
    }
}

var permitir3 = true;
function verifUnidades(unidades) {
    if(unidades.value=='')
    {
        let alerta = '<p style="color:orange">Las unidades no pueden estar vacio<p>';
        $('#aviso-unidades').html(alerta);
        $('aviso-unidades').show();
        permitir3 = false;
    }
    else{
        $('aviso-unidades').hide();
        if(unidades.value != "" && unidades.value >= 0)
        {
            let alerta = '<p style="color:black">El numero de unidades es valido<p>';
            $('#aviso-unidades').html(alerta);
            $('aviso-unidades').show();
            permitir3 = true;
        }
        else
        {
            if(unidades.value < 0)
            {
                let alerta = '<p style="color:orange">El numero de unidades es menor que 0!!!<p>';
                $('#aviso-unidades').html(alerta);
                $('aviso-unidades').show();
                permitir3 = false;
            }
            else
            {
                $('aviso-unidades').hide();
                //permitir3 = false;
            }
           
        }
    }
}
var permitir4 = true; 
function verifMarca(marca) {
    if(marca.value=='')
    {
        let alerta = '<p style="color:orange">La marca no puede estar vacio<p>';
        $('#aviso-marca').html(alerta);
        $('aviso-marca').show();
        permitir4 = false;
    }
    else{
        $('aviso-marca').hide();
        if(marca.value!="")
        {
            let alerta = '<p style="color:black">La marca esta bien<p>';
            $('#aviso-marca').html(alerta);
            $('aviso-marca').show();    
            permitir4 = true;
        }
        $('aviso-marca').hide();
    }
}

var permitir5 = true; 
function verifModelo(modelo) {
    if(modelo.value=='')
    {
        let alerta = '<p style="color:orange">El modelo no puede estar vacio<p>';
        $('#aviso-modelo').html(alerta);
        $('aviso-modelo').show();
        permitir5 = false;
    }
    else{
        $('aviso-modelo').hide();
        if(modelo.value!="" && modelo.value.length<25)
        {
            let alerta = '<p style="color:black">El modelo esta bien<p>';
            $('#aviso-modelo').html(alerta);
            $('aviso-modelo').show();    
            permitir5 = true;
        }
        else
        {
            if(modelo.value.length>25)
            {
                let alerta = '<p style="color:orange">El modelo no puede tener mas de 25 caracteres<p>';
                $('#aviso-modelo').html(alerta);
                $('aviso-modelo').show();
                permitir5 = false;
            }
        }
        $('aviso-modelo').hide();
    }
}

var permitir6 = true; 
function verifDetalles(detalles) {
    if(detalles.value=="" || detalles.value.length<=250)
    {
        let alerta = '<p style="color:black">Los detalles estan bien<p>';
        $('#aviso-detalles').html(alerta);
        $('aviso-detalles').show();    
        permitir6 = true;
    }
    else
    {
        if(detalles.value.length>=250)
        {
            let alerta = '<p style="color:orange">Los detalles no pueden tener mas de 250 caracteres<p>';
            $('#aviso-detalles').html(alerta);
            $('aviso-detalles').show();
            permitir6 = false;
        }
    }
    $('aviso-detalles').hide();
}

var permitir7 = true; 
function verifImagen(imagen) {
    if(imagen.value=="")
    {
        imagen.value = "img/imagen-default.png";
        let alerta = '<p style="color:black">Los detalles estan bien<p>';
        $('#aviso-imagen').html(alerta);
        $('aviso-imagen').show();    
        permitir7 = true;
    }
    $('aviso-imagen').hide();
}

function verifTodos()
{
    if(permitir1 == true && permitir12 == true && permitir2 == true && permitir3 == true && permitir4 == true && permitir5 == true && permitir6 == true && permitir7 == true)
    {
        return true;
    }
}

$(document).ready(function(){
    let edit = false;

    //let JsonString = JSON.stringify(baseJSON,null,2);
    //$('#description').val(JsonString);
    $('#product-result').hide();
    listarProductos();

    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function(response) {
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                const productos = JSON.parse(response);
            
                // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                if(Object.keys(productos).length > 0) {
                    // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                    let template = '';

                    productos.forEach(producto => {
                        // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                        let descripcion = '';
                        descripcion += '<li>precio: '+producto.precio+'</li>';
                        descripcion += '<li>unidades: '+producto.unidades+'</li>';
                        descripcion += '<li>modelo: '+producto.modelo+'</li>';
                        descripcion += '<li>marca: '+producto.marca+'</li>';
                        descripcion += '<li>detalles: '+producto.detalles+'</li>';
                    
                        template += `
                            <tr productId="${producto.id}">
                                <td>${producto.id}</td>
                                <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                <td><ul>${descripcion}</ul></td>
                                <td>
                                    <button class="product-delete btn btn-danger" onclick="eliminarProducto()">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    $('#products').html(template);
                }
            }
        });
    }

    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: './backend/product-search.php?search='+$('#search').val(),
                data: {search},
                type: 'GET',
                success: function (response) {
                    if(!response.error) {
                        // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                        const productos = JSON.parse(response);
                        
                        // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
                        if(Object.keys(productos).length > 0) {
                            // SE CREA UNA PLANTILLA PARA CREAR LAS FILAS A INSERTAR EN EL DOCUMENTO HTML
                            let template = '';
                            let template_bar = '';

                            productos.forEach(producto => {
                                // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                                let descripcion = '';
                                descripcion += '<li>precio: '+producto.precio+'</li>';
                                descripcion += '<li>unidades: '+producto.unidades+'</li>';
                                descripcion += '<li>modelo: '+producto.modelo+'</li>';
                                descripcion += '<li>marca: '+producto.marca+'</li>';
                                descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td><a href="#" class="product-item">${producto.nombre}</a></td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;

                                template_bar += `
                                    <li>${producto.nombre}</il>
                                `;
                            });
                            // SE HACE VISIBLE LA BARRA DE ESTADO
                            $('#product-result').show();
                            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                            $('#container').html(template_bar);
                            // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                            $('#products').html(template);    
                        }
                    }
                }
            });
        }
        else {
            $('#product-result').hide();
        }
    });

    $('#product-form').submit(e => {
        e.preventDefault();

        // SE CONVIERTE EL JSON DE STRING A OBJETO
        //let postData = JSON.parse( $('#description').val() );
        // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
        const postData = {
            nombre: $('#name').val(),
            precio: parseFloat($('#precio').val()),
            unidades: parseFloat($('#unidades').val()),
            modelo: $('#modelo').val(),
            marca: $('#marca').val(),
            detalles: $('#detalles').val(),
            imagen: $('#imagen').val(),
            id: $('#productId').val()
        };
        /**
         * AQUÍ DEBES AGREGAR LAS VALIDACIONES DE LOS DATOS EN EL JSON
         * --> EN CASO DE NO HABER ERRORES, SE ENVIAR EL PRODUCTO A AGREGAR
         **/
        var resultado = verifTodos();
        if (resultado == true)
        { 

            let url = edit === false ? './backend/product-add.php' : './backend/product-edit.php';
            console.log(postData, url);
            postDataR = JSON.stringify(postData);
            productoJsonString = JSON.stringify(postData,null,2);
            console.log(productoJsonString);
            $.post(url, postData, (response) => {
                console.log(response);
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                let respuesta = JSON.parse(response);
                // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                // SE REINICIA EL FORMULARIO
                $('#name').val('');
                $('#detalles').val('');
                $('#marca').val('');
                $('#modelo').val('');
                $('#unidades').val('');
                $('#precio').val('');
                $('#imagen').val('');
                // SE HACE VISIBLE LA BARRA DE ESTADO
                $('#product-result').show();
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                $('#container').html(template_bar);
                // SE LISTAN TODOS LOS PRODUCTOS
                listarProductos();
                // SE REGRESA LA BANDERA DE EDICIÓN A false
                edit = false;
            });
        }
        else
        {
            alert("Datos Erroneos, Por Favor Valide Los Datos");
        }
        });
        
    $(document).on('click', '.product-delete', (e) => {
        if(confirm('¿Realmente deseas eliminar el producto?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productId');
            $.post('./backend/product-delete.php', {id}, (response) => {
                $('#product-result').hide();
                listarProductos();
            });
        }
    });

    $(document).on('click', '.product-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productId');
        $.post('./backend/product-single.php', {id}, (response) => {
            // SE CONVIERTE A OBJETO EL JSON OBTENIDO
            let producto = JSON.parse(response);
            // SE INSERTAN LOS DATOS ESPECIALES EN LOS CAMPOS CORRESPONDIENTES
            $('#name').val(producto.nombre);
            $('#precio').val(producto.precio);
            $('#unidades').val(producto.unidades);
            $('#modelo').val(producto.modelo);
            $('#marca').val(producto.marca);
            $('#detalles').val(producto.detalles);
            $('#imagen').val(producto.imagen);
            // EL ID SE INSERTA EN UN CAMPO OCULTO PARA USARLO DESPUÉS PARA LA ACTUALIZACIÓN
            $('#productId').val(producto.id);
            // SE ELIMINA nombre, eliminado E id PARA PODER MOSTRAR EL JSON EN EL <textarea>
            // delete(producto.nombre);
            // delete(producto.precio);
            // delete(producto.unidades);
            // delete(producto.modelo);
            // delete(producto.marca);
            // delete(producto.imagen);
            // delete(producto.detalles);
            // delete(producto.eliminado);
            // delete(producto.id);
            // SE CONVIERTE EL OBJETO JSON EN STRING
            //let JsonString = JSON.stringify(product,null,2);
            // SE MUESTRA STRING EN EL <textarea>
            //$('#description').val(JsonString);
            
            // SE PONE LA BANDERA DE EDICIÓN EN true
            edit = true;
        });
        e.preventDefault();
    });    
});