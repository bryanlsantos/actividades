<?php

abstract class DataBase
{
    protected $conexion;
    
    public function __construct($cadena)
    {
    #Su constructor debe inicializar el objeto conexion con los datos de
    #conexión de tu BD
    #El parámetro recibido en el constructor debe usarse como nombre de la
    #base de datos.
    $this->conexion = @mysqli_connect(
        'localhost',
        'root',
        'Elkomander',
        "{$cadena}"     );
    }
}
?>